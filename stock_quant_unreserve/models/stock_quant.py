from odoo import _, api, models
from odoo.exceptions import RedirectWarning
from odoo.tools.float_utils import float_compare


class StockQuant(models.Model):
    _inherit = 'stock.quant'

    @api.model
    def _update_reserved_quantity(self, product_id, location_id, quantity, lot_id=None, package_id=None, owner_id=None, strict=False):
        self = self.sudo()
        rounding = product_id.uom_id.rounding
        quants = self._gather(product_id, location_id, lot_id=lot_id, package_id=package_id, owner_id=owner_id, strict=strict)


        if float_compare(quantity, 0, precision_rounding=rounding) < 0:
            # if we want to unreserve
            available_quantity = sum(quants.mapped('reserved_quantity'))
            if float_compare(abs(quantity), available_quantity, precision_rounding=rounding) > 0:
                action_fix_unreserve = self.env.ref(
                    'stock_quant_unreserve.stock_quant_stock_move_line_desynchronization', raise_if_not_found=False)
                if action_fix_unreserve and self.user_has_groups('base.group_system'):
                    raise RedirectWarning(
                        _("""It is not possible to unreserve more products of %s than you have in stock.
The correction could unreserve some operations with problematic products.""") % product_id.display_name,
                        action_fix_unreserve.id,
                        _('Automated action to fix it'))
        return super()._update_reserved_quantity(product_id, location_id, quantity, lot_id=lot_id, package_id=package_id, owner_id=owner_id, strict=strict)
