{
    "name": "Stock Quant Unreserve",
    "version": "16.0.0.1.0",
    "author": "HomebrewSoft",
    "website": "https://homebrewsoft.dev",
    "license": "LGPL-3",
    "depends": [
        "stock",
    ],
    "data": [
        # security
        # data
        "data/ir_actions_server.xml",
        # reports
        # views
    ],
}
